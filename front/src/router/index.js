import { createRouter, createWebHashHistory } from 'vue-router'
import Crearpaciente from "../components/Crearpaciente.vue";
import Listapaciente from "../components/Listapaciente.vue";
import Registroeventos from "../components/Registroeventos.vue";
import listaEventos from "../components/listaEventos.vue";


const routes = [
  {
    path: "/crear",
    name: "crearE",
    component: Crearpaciente,
  },
  {
    path: "/listar",
    name: "listarE",
    component: Listapaciente,
  },
  {
    path: "/Registrar",
    name: "registrarE",
    component: Registroeventos,
  },
  {
    path: "/listarEventos",
    name: "registrarN",
    component: listaEventos,
  },
  
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
