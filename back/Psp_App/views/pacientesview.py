from django.db.models.base import ModelStateFieldsCacheDescriptor
from rest_framework.response import Response
from rest_framework import views
from Psp_App.models.pacientes import pacientes
from Psp_App.serializers.pacientes_serializer import Mostrarpacientes_serializer, Crearpacientes_serializer


class pacientesview(views.APIView):
    def get (self, requerest):
        lista_pacientes = pacientes.objects.all()
        serializers = Mostrarpacientes_serializer(lista_pacientes, many=True)
        return Response(serializers.data, 200)

 
    def post(self, request):
        datos_json = request.data
        serializers = Crearpacientes_serializer(data=datos_json)
        if serializers.is_valid():
            serializers.save()
            return Response({"mensaje": "Paciente creado"}, 200)       
        else:
            return Response(serializers.errors, 405)

    
    def delete(self, request, pk):
        try:
            Psp_App = pacientes.objects.get(pk=pk)
            Psp_App.delete()
            return Response({"mensaje": "Paciente borrado"}, 200)
        except:
            return Response({"mensaje": "Paciente no existe"}, 400)