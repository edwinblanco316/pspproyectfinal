from rest_framework import views
from rest_framework.response import Response
from Psp_App.models.nota import Nota
from Psp_App.serializers.nota_serializer import NotaSerializer

class NotaView(views.APIView):
    def get(self, request):
        lista_notas = Nota.objects.all()
        serializer = NotaSerializer(lista_notas, many = True)
        return Response(serializer.data, 200)

    def post(self, request):
        datos_jason = request.data
        serializer = NotaSerializer(data = datos_jason)
        if serializer.is_valid():
            serializer.save()
            return Response({"mensaje": "Nota creada"}, 200)
        else:
            return Response(serializer.errors ,405)

    def delete(self, request, pk):
        try:
            estudiante = Nota.objects.get(pk=pk)
            estudiante.delete()
            return Response({"Mensaje":"Nota borrada..."}, 200)
        except:
            return Response({"Mensaje":"La nota no existe"}, 400)

        
        

