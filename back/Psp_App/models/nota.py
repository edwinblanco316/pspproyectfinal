from django.db import models
from django.db.models.base import Model
from django.db.models.fields import CharField
from django.utils import translation

class Nota(models.Model):
    id_nota = models.AutoField(primary_key=True)
    fecha = models.DateTimeField(auto_now= True, null=False)
    usuario = models.CharField(max_length=60, null=False)
    profesional_en_gestion = models.CharField(max_length=100)
    fecha_proxima_gestion = models.DateTimeField()
    fecha_ultima_gestion = models.DateTimeField()
    estado_en_eps= models.CharField(max_length=100)
    estado_tratamiento= models.CharField(max_length=115)
    nota= models.CharField(max_length=500)
    diagnostico = models.CharField(max_length=110)
    tratamiento= models.CharField(max_length=110)
