from django.db.models import fields
from rest_framework import serializers
from Psp_App.models.nota import Nota

class NotaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Nota
        fields = ["id_nota","fecha", "usuario", "profesional_en_gestion", "fecha_proxima_gestion", 
        "fecha_ultima_gestion", "estado_en_eps", "estado_tratamiento", "nota", "diagnostico", "tratamiento"]
