from rest_framework import serializers
from Psp_App.models.pacientes import pacientes


class Crearpacientes_serializer(serializers.ModelSerializer):
    class Meta:
        model = pacientes
        fields = ["id", "Nombres", "Apellidos", "No_documento", "Direccion", "Telefono", "Telefono1", "Ciudad", "Nombre_responsable", "Telefono_responsable", "Telefono1_responsable"]

     
class Mostrarpacientes_serializer(serializers.ModelSerializer):
    class Meta:
        model = pacientes
        fields = ["id", "Nombres", "Apellidos", "No_documento", "Direccion", "Telefono", "Telefono1", "Ciudad", "Nombre_responsable", "Telefono_responsable", "Telefono1_responsable"] 

